$(document).ready(function(){
    
$("#tabel-barang").DataTable({
    "paging":true,
    "processing":true,
    "serverSide":true,
    "scrollY":"280px",
    "scrollCollapse":true,
    "order":[],
    "searching":true,
    "lengthMenu":[[10,20,50,-1],[10,20,50,'all data']],
    "ajax":{
        url:site_url+"barang/fetch_barang",
        type:"POST",
        dataType:"json",
    },
    "columns":[
        {'data':'no'},
        {'data':'foto'},
        {'data':'nama_barang'},
        {'data':'harga_beli'},
        {'data':'harga_jual'},
        {'data':'stok'},
        {'data':'action'},
    ],
    "columnDefs":[
        {
            'targets':[0,1,6],
            'orderable':false,
        },
    ],
});

});//document ready end tag



//some function here
function reload_table()
{
    $('#tabel-barang').DataTable().ajax.reload(null,false);

}

function hapus_data_barang(id_barang)
{
    var url = site_url + "barang/delete_data/";
    if(confirm('Yakin Hapus?')){
        $.ajax({
            url : site_url + 'barang/delete_data/' +  id_barang,  
            type: "POST",
            dataType:"json",
            success:function(data){
                if(data.status=='true'){
                    alert('data berhasil di hapus');
                    reload_table();
                }
            }
        });
    }   
}


function edit_data(id_barang)
{
    $('.form')[0].reset();
    
    $.ajax({
        url : site_url + 'barang/edit_data/' + id_barang,
        type: "GET",
        dataType: "JSON",
        success:function(data)
        {

            // ingin beri nilai untuk input hidden edit-errors supaya bisa langsung muncul
            $('#edit-errors').val(data.response_edit);
            
            $('[name="id_barang"]').val(data.data_barang.id_barang);
            $('[name="nama_barang_lama"]').val(data.data_barang.nama_barang);
            $('[name="nama_barang_baru"]').val(data.data_barang.nama_barang);
            $('[name="harga_beli_baru"]').val(data.data_barang.harga_beli);
            $('[name="harga_jual_baru"]').val(data.data_barang.harga_jual);
            $('[name="stok_baru"]').val(data.data_barang.stok);
            $('#foto').attr('src',site_url +'assets/foto/barang/'+data.data_barang.foto);
            // alert(data.data_barang.foto);
            $('[name="nama_foto_lama"]').val(data.data_barang.foto);

            // // $('[name="stok"]').val(data.data_barang.stok); 
            $('#modal-edit').modal('show');
           
            //ganti action dari form nya | gak jadi karena gak full ajax. gausah
            // $('#form-edit').attr('action',site_url+'barang/update_data');
            // $('.modal-header').text('Edit Barang'); // Set title to Bootstrap modal title
        },
    });
}

function reset_form()
{
    $('.form')[0].reset();
}