-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jan 25, 2018 at 10:00 PM
-- Server version: 10.1.25-MariaDB
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_nutech`
--

-- --------------------------------------------------------

--
-- Table structure for table `barang`
--

CREATE TABLE `barang` (
  `id_barang` int(11) NOT NULL,
  `nama_barang` varchar(255) NOT NULL,
  `harga_beli` int(11) NOT NULL,
  `harga_jual` int(11) NOT NULL,
  `stok` int(11) NOT NULL,
  `foto` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `barang`
--

INSERT INTO `barang` (`id_barang`, `nama_barang`, `harga_beli`, `harga_jual`, `stok`, `foto`) VALUES
(5, 'Tamiya Magnum Saber', 790000, 920000, 15, '41MHZ3AEP4L__SX355_.jpg'),
(8, 'figma archetype', 320000, 420000, 18, '928ae0066460c9183f404d1b202fd3b7.jpg'),
(9, 'nintendo switch', 6200000, 7300000, 31, '1513380303756712793.jpg'),
(10, 'yugioh trading card (yugi-deck)', 35000, 45000, 20, 'noimage.jpg'),
(11, 'Laptop Asus', 4500000, 4800000, 10, 'asus-a455ld-wx162d-i5-5200u-or-gt820m-or-2gb-or-500gb-or-dos-black-1.jpg'),
(12, 'Laptop Lenovos', 4200000, 4720000, 11, 'lenovo-laptop-essential-g40-silver-main-new.png'),
(13, 'Gundam IBO Barbatos HG', 290000, 280000, 1, 'bandai-iron-blooded-orphans-gundam-barbatos-6th-form-hg-1488195819-83698902-3d6c060a6c988c3f316b22583e9882f1-product.jpg'),
(14, 'Figma Archetype Men', 320000, 380000, 1222, '928ae0066460c9183f404d1b202fd3b71.jpg'),
(15, 'Tas Eiger', 125000, 150000, 20, 'Tas_Selempang_Slempang_Eiger_TP_Scoria_Waistbag.jpg'),
(16, 'Jam tangan casio', 354000, 370000, 80, 'casio_jam-tangan-casio-ae-1000w-1a_full03.jpg'),
(17, 'Razer Mechanical Key', 1700000, 1850000, 122, '41kAj8TB4RL__SL500_AC_SS350_.jpg'),
(18, 'Logitech G102 Mouse', 290000, 320000, 10, 'g102-g203.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `barang`
--
ALTER TABLE `barang`
  ADD PRIMARY KEY (`id_barang`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `barang`
--
ALTER TABLE `barang`
  MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
