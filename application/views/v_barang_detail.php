<style>
    #foto-barang{
        width:250px;
        height:250px;
    }

    
</style>




<div class="content-wrapper">
    <div class="container-fluid">
        <div class=" card-mb-3">
            <div class="card-header">  
                <div class="row">
                    <h4 class="col-md-6">
                        <?php echo $data_barang['nama_barang']; ?>
                    </h4>
                    <div class="col-md-6">
                        <span class="float-md-right">

                        </span>
                    </div>    
                </div>
            </div>
           


                
            <div class="card-body">
                
                    <div class="row col-md-12">
                     <h5 class="col-md-3"> Harga beli </h5>
                     <h5 class="col-md-4"> : <?php echo $data_barang['harga_beli']; ?></h5> 
                    </div>

                    <div class="row col-md-12">
                     <h5 class="col-md-3"> Harga jual </h5>
                     <h5 class="col-md-4"> : <?php echo $data_barang['harga_jual']; ?></h5> 
                    </div>

                    <div class="row col-md-12">
                     <h5 class="col-md-3"> Jumlah stok </h5>
                     <h5 class="col-md-4"> : <?php echo $data_barang['stok']; ?></h5> 
                    </div>

                    <hr>

                    <div class="row col-md-12">
                     <h6 class="col-md-3"> Foto Barang </h6> 
                    </div>
                
                    <div class="col-md-12">                            
                        <img src="<?php echo base_url().'assets/foto/barang/'.$data_barang['foto'];?>" id="foto-barang"class="img-thumbnail" alt="">
                    </div>
            </div>


    </div>
</div>




<!-- Jquery Javascript -->
<script src="<?php echo base_url()?>assets/sbadmin/vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Javascript -->
<script src="<?php echo base_url()?>assets/sbadmin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="<?php echo base_url()?>assets/sbadmin/vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- plugin untuk datables -->
<script src="<?php echo base_url()?>assets/sbadmin/vendor/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url()?>assets/sbadmin/vendor/datatables/dataTables.bootstrap4.js"></script>

<script>
    var site_url = "<?php echo site_url();?>";
</script>

<!-- main script datatable barang -->
<script src="<?php echo base_url()?>assets/main-js/barang.js"></script>



