
<!-- modal insert -->
<div class="modal fade" id="modal-insert" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="class modal-dialog" role="document">
        <div class="class modal-content">
            <div class="class modal-header">
                <h4>Input Barang Baru</h4> 
            </div>
            <div class="class modal-body">
                <?php echo form_open_multipart('barang/insert_data',array('class'=>'form','id'=>'form-insert'));?>
                <div class="form-group">
                    <div class="row">
                        <label for="nama_barang" class="col-sm-4 col-form-label"> Nama Barang </label>
                        <input type="text" name="nama_barang" class="form-control col-sm-7" placeholder="Nama Barang" value="<?php echo set_value('nama_barang'); ?>"> 
                    </div>
                    <div class="offset-md-4"id="msg_nama_barang"><?php echo form_error('nama_barang');?></div>
                </div>
                
                <div class="form-group">
                    <div class="row">
                        <label for="harga_beli" class="col-sm-4 col-form-label"> Harga Beli </label>
                        <input type="text" name="harga_beli" class="form-control col-sm-7" placeholder="Harga Beli" value="<?php echo set_value('harga_beli'); ?>"> 
                    </div>
                    <div class="offset-md-4"id="msg_harga_beli"><?php echo form_error('harga_beli');?></div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="harga_jual" class="col-sm-4 col-form-label"> Harga Jual </label>
                        <input type="text" name="harga_jual" class="form-control col-sm-7" placeholder="Harga Jual" value="<?php echo set_value('harga_jual'); ?>"> 
                    </div>
                    <div class="offset-md-4"id="msg_harga_jual"><?php echo form_error('harga_jual');?></div>
                </div>
                <div class="form-group">
                    <div class="row">
                        <label for="stok" class="col-sm-4 col-form-label"> Stok </label>
                        <input type="text" name="stok" class="form-control col-sm-7" placeholder="Stok" value="<?php echo set_value('stok'); ?>"> 
                    </div>
                    <div class="offset-md-4"id="msg_stok"><?php echo form_error('stok');?></div>
                </div>
                <div class="form-group">
                    <div class="class row">
                        <label for="stok" class="col-sm-4 col-form-label"> Foto </label>
                        <input type="file" name="foto" class="form-control col-sm-7" >
                    </div>
                    <div class="offset-md-4"id="msg_foto"><?php echo form_error('foto');?></div>
                </div>

            </div>
            <div class="class modal-footer">
                <button type="submit" class="btn btn-primary"> Simpan </button>
                <button type="button" class="btn btn-cancel" data-dismiss="modal"> Batal </button>
            </div>
            <?php echo form_close();?>
        </div>
    </div>
</div>


<div class="content-wrapper">
    <div class="class container-fluid">
    <div class="class card-mb-3">
        <div class="class card-header">  
            <h3>Tabel Barang</h3>
                <button data-toggle="modal" onclick="reset_form()" data-target="#modal-insert" id="insert-barang" class="btn btn-md btn-primary"> <i class="fa fa-fw fa-plus-circle" ></i> Tambah Data Barang </button>                
                <button id="reload-barang" class="btn btn-md btn-success" onclick="reload_table()">  <i class="fa fa-fw fa-refresh" ></i> Reload Tabel </button>
        </div>
        <div class="class card-body">
            <table class="table table-bordered" id="tabel-barang" width="100%" cellspasing="0">
                <thead>
                    <tr>
                        <td style="width:5%" > no </td>
                        <td style="width:5%" > Foto </td>
                        <td style="width:20%"> Nama Barang </td>
                        <td style="width:20%"> Harga Beli </td>
                        <td style="width:20%"> Harga Jual </td>
                        <td style="width:10%"> Stok </td>
                        <td style="width:20%"> Action </td>
                    </tr>
                </thead>
            </table>
        </div>
    </div>
</div>




<!-- edit modal -->
<div class="modal fade" id="modal-edit" tabindex="-1" role="dialog" aria-hidden="true">
    <div class="class modal-dialog" role="document">
        <div class="class modal-content">
            <div class="class modal-header">
                <h4>Edit Data Barang</h4> 
            </div>
            <div class="modal-body">
                <?php echo form_open_multipart('barang/update_data',array('class'=>'form','id'=>'form-edit'));?>
                    <input id="hidden" type="hidden" name="id_barang" value="">
                    
                    <input type="hidden" name="nama_barang_lama" id="nama_barang_lama" value="">
                    <div class="form-group">
                        <div class="row">
                            <label for="nama_barang" class="col-sm-4 col-form-label"> Nama Barang </label>
                            <input type="text" name="nama_barang_baru" class="form-control col-sm-7" placeholder="Nama Barang" value="<?php echo set_value('nama_barang_baru'); ?>"> 
                        </div>
                        <div class="offset-md-4"id="msg_nama_barang_baru"><?php echo form_error('nama_barang_baru');?></div>
                    </div>
                    
                    <div class="form-group">
                        <div class="class row">
                            <label for="harga_beli" class="col-sm-4 col-form-label"> Harga Beli </label>
                            <input type="text" name="harga_beli_baru" class="form-control col-sm-7" placeholder="Harga Beli" value="<?php echo set_value('harga_beli_baru'); ?>"> 
                        </div>
                        <div class="offset-md-4"id="msg_harga_beli_baru"><?php echo form_error('harga_beli_baru');?></div>
                    </div>
                    <div class="form-group">
                        <div class="class row">
                            <label for="harga_jual" class="col-sm-4 col-form-label"> Harga Jual </label>
                            <input type="text" name="harga_jual_baru" class="form-control col-sm-7" placeholder="Harga Jual" value="<?php echo set_value('harga_jual_baru'); ?>"> 
                        </div>
                        <div class="offset-md-4"id="msg_harga_jual_baru"><?php echo form_error('harga_jual_baru');?></div>
                    </div>
                    <div class="form-group">
                        <div class="class row">
                            <label for="stok" class="col-sm-4 col-form-label"> Stok </label>
                            <input type="text" name="stok_baru" class="form-control col-sm-7" placeholder="Stok_baru" value="<?php echo set_value('stok_baru'); ?>"> 
                        </div>
                        <div class="offset-md-4"id="msg_stok_baru"><?php echo form_error('stok_baru');?></div>
                    </div>
                    <div class="form-group">
                        <div class="row">
                            <label for="stok" class="col-sm-4 col-form-label"> Upload Foto Baru </label>
                            <input type="file" name="foto_baru" class="form-control col-sm-7" >
                        </div>
                        <div class="offset-md-4"id="msg_foto"><?php echo form_error('foto');?></div>
                    </div>

                    <div class="">
                        <div class="row">
                            <div class="col-sm-4"></div>
                            <div class="col-sm-7">
                                <input type="hidden" name="nama_foto_lama" value="">
                                <img id="foto" name="foto" src="" class="img-thumbnail" alt="">
                            </div>
                        </div>
                    </div>


                    </div>
                    <div class="class modal-footer">
                        <button type="submit" class="btn btn-primary"> Simpan </button>
                        <button type="button" onclick="reset_form()" class="btn btn-default" data-dismiss="modal"> Batal </button>
                    </div>
                <?php echo form_close(); ?>
            </div>
        </div>
    </div>
</div>








<!-- Jquery Javascript -->
<script src="<?php echo base_url()?>assets/sbadmin/vendor/jquery/jquery.min.js"></script>
<!-- Bootstrap Javascript -->
<script src="<?php echo base_url()?>assets/sbadmin/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- Core plugin JavaScript-->
<script src="<?php echo base_url()?>assets/sbadmin/vendor/jquery-easing/jquery.easing.min.js"></script>
<!-- plugin untuk datables -->
<script src="<?php echo base_url()?>assets/sbadmin/vendor/datatables/jquery.dataTables.js"></script>
<script src="<?php echo base_url()?>assets/sbadmin/vendor/datatables/dataTables.bootstrap4.js"></script>

<script>
    var site_url = "<?php echo site_url();?>";
    var response_validation = "<?php //echo $response; ?>"
$(document).ready(function(){
    
});


</script>

<!-- main script datatable barang -->
<script src="<?php echo base_url()?>assets/main-js/barang.js"></script>



