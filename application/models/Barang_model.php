<?php
defined('BASEPATH') OR exit('No direct script access allowed');


class Barang_model extends CI_Model
{
    public function __construct()
    {
        parent::__construct();
        $this->load->database();
    }


    var $table          = 'barang';
    var $column_select  = array('id_barang','nama_barang','harga_beli','harga_jual','stok','foto');
    var $column_order   = array('id_barang','nama_barang','harga_beli','harga_jual','stok','foto');

    public function query()
    {
        $this->db->select($this->column_select);
        $this->db->from($this->table);

        if(isset($_POST['search']['value'])){
            $this->db->like('nama_barang',$_POST['search']['value']);
        }
        if(isset($_POST['order'])){
            $this->db->order_by($this->column_order[$_POST['order']['0']['column']],$_POST['order']['0']['dir']);
        } else {
            $this->db->order_by('id_barang','DESC');
        }

    }

    public function make_datatables()
    {
        $this->query();
        
        //menentukan panjang record yang akan ditampilkan nih 
        if($_POST['length'] != -1 )
        {
            $this->db->limit($_POST['length'],$_POST['start']);
        }

        $query = $this->db->get();
        return $query->result_array(); //cetak kedalam array
    }

    function count_filtered_data()
    {
        $this->query();
        $query = $this->db->get();
        return $query->num_rows();
    }

    function count_all_data()
    {
        $this->db->select('*');
        $this->db->from($this->table);
        return $this->db->count_all_results();
    }

    function get_data($key,$value)
    {
        $query = $this->db->get_where('barang',array($key=>$value));
        return $query->row_array();
    }
    
    
    function insert_data($data_baru)
    {
        $this->db->insert('barang',$data_baru);

    }

    function update_data($id_barang,$data_baru)
    {
        $this->db->where('id_barang',$id_barang);
        $this->db->update('barang',$data_baru);
    }

    function delete_data($id_barang)
    {
        $this->db->where('id_barang',$id_barang);
        $this->db->delete('barang');
    }


}


?>