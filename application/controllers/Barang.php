<?php 
defined('BASEPATH') OR exit('No direct script access allowed');


class Barang extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->model('barang_model');
    }

    public function index()
    {
        $data = array(
            'response_insert'     => 'insert',
            'response_edit'   => 'edit',
            'content'   => 'v_barang',
            'title'     => 'Barang'
        );
        $this->load->view('layouts/v_layout',$data);
    }

    public function fetch_barang()
    {
        $get_all_barang = $this->barang_model->make_datatables();
        
        $no = 1 ;
        foreach($get_all_barang as $row){
            $sub_array = array();
            $sub_array['no']            = $no;
            $sub_array['id_barang']     = $row['id_barang'];
            $sub_array['nama_barang']   = $row['nama_barang'];
            $sub_array['harga_beli']    = $row['harga_beli'];
            $sub_array['harga_jual']    = $row['harga_jual'];
            $sub_array['stok']          = $row['stok'].' unit';
            $sub_array['foto']          = '<a href="'.base_url().'barang/get_data/'.$row['id_barang'].'"><img src="'.base_url().'assets/foto/barang/'.$row['foto'].'" style="height:80px; width:80px;"> </a>';
            $sub_array['action']        = '<button type="button" data-toggle="modal" onclick="edit_data('.$row['id_barang'].')" class="btn btn-sm btn-success"> <i class="fa fa-fw fa-pencil"></i> Edit </button><a href="javascript:void(0)"> <button type="button" onclick="hapus_data_barang('.$row['id_barang'].')"class="btn btn-sm btn-danger"> <i class="fa fa-fw fa-trash"></i> Delete </button> </a>';
            
            $data_barang[] = $sub_array;
            $no++;
        }

        $output = array(
            'draw'              => intval($_POST['draw']),
            'recordsTotal'      => $this->barang_model->count_all_data(),
            'recordsFiltered'   => $this->barang_model->count_filtered_Data(),
            'data'              => $data_barang
        );
        echo json_encode($output);
    }

    public function insert_data()
    {
        $this->load->library('form_validation');

        $v = $this->form_validation;
        $v->set_rules('foto','Foto','trim|callback_cek_foto');
        
        $v->set_rules('nama_barang','nama barang','trim|required|is_unique[barang.nama_barang]',array(
            'required'  => '*nama barang harus di isi',
            'is_unique' => '*nama barang yang sama sudah ada',
        ));
        $v->set_rules('harga_beli','nama barang','trim|numeric|required',array(
            'numeric'   => '*hanya boleh di isi dengan angka',
            'required'  => '*tidak boleh di kosongkan'
        ));
        $v->set_rules('harga_jual','nama barang','trim|numeric|required',array(
            'numeric'   => '*hanya boleh di isi dengan angka',
            'required'  => '*tidak boleh di kosongkan'
        ));
        $v->set_rules('stok','nama barang','trim|numeric|required',array(
            'numeric'   => '*hanya boleh di isi dengan angka',
            'required'  => '*tidak boleh di kosongkan'
        ));
        
        if($v->run() === false ){ 

            $data = array(
                'response_insert'  => 'insert-error',
                'content'   => 'v_barang',
                'title'     => 'Barang'
            );
            $this->load->view('layouts/v_layout',$data);
        
        } else {

            $i = $this->input;
            $config = array(
                'upload_path'   => './assets/foto/barang/',
                'allowed_types' => 'jpg|png',
                'max_size'      => '100'//kb,
            );

            $this->load->library('upload',$config);
            if($this->upload->do_upload('foto')){
                $foto        = $this->upload->data()['file_name'];
                $data_baru = array(
                    'nama_barang'   => $i->post('nama_barang'),
                    'harga_beli'    => $i->post('harga_beli'),
                    'harga_jual'    => $i->post('harga_jual'),
                    'stok'          => $i->post('stok'),
                    'foto'          => $foto,
                );
                $this->barang_model->insert_data($data_baru);
                redirect('barang');
                $data = array(
                    'response'  => 'insert-error',
                    'content'   => 'v_barang',
                    'title'     => 'Barang'
                );
                $this->load->view('layouts/v_layout',$data);
            
            } else {
                $data = array(
                    'response'  => 'insert-error',
                    'content'   => 'v_barang',
                    'title'     => 'Barang'
                );
                $this->load->view('layouts/v_layout',$data);
            }          
        }
    }

    public function cek_foto($foto)
    {
        if(empty($_FILES['foto']['name'])|| $_FILES['foto']['size'] == 0 ){
            //jika file nya kosong
            // ya sebenernya gapapa sih kalo kosong juga bisa di tambahin entar 
            $this->form_validation->set_message('cek_foto','Harus pilih foto');
            return false;
        } else {
            //jika file ada
            $allowed_extensions = array(
                'jpg','png','JPG','PNG','JPEG','jpeg'
            );
            $allowed_types = array(
                IMAGETYPE_PNG,IMAGETYPE_JPEG
            );
            $extension  = pathinfo($_FILES['foto']['name'],PATHINFO_EXTENSION);
            $type       = exif_imagetype($_FILES['foto']['tmp_name']);

            //check rules
            if(!in_array($type,$allowed_types)){
                $this->form_validation->set_message('cek_foto','file harus bertipe JPG/PNG');
                return false;
            }
            if(!in_array($extension,$allowed_extensions)){
                $this->form_validation->set_message('cek_foto','file harus berekstensi JPG/PNG');
                return false;
            }
            if($_FILES['foto']['size'] > 100000 ){
                $this->form_validation->set_message('cek_foto','file tidak boleh melebihi 100 KB');
                return false;
            }
        } 
        return true;
    }


    public function get_data($id_barang)
    {
        $data_barang = $this->barang_model->get_data('id_barang',$id_barang);
        $data = array(
            'content'       => 'v_barang_detail',
            'title'         => 'Detail barang',
            'data_barang'   => $data_barang
        );
        $this->load->view('layouts/v_layout',$data);
    }

    public function delete_data($id_barang)
    {
        $this->barang_model->delete_data($id_barang);
        echo json_encode(array('status'=>'true'));
    }
    
    public function edit_data($id_barang)
    {
        $data_barang = $this->barang_model->get_data('id_barang',$id_barang);
        $response = array(
            'response_edit'   => 'edit',
            'status'     => 'success',
            'data_barang'=> $data_barang
        );
        echo json_encode($response);
    }
    






















    public function cek_nama_barang($str,$str2){
        //bikin query untuk cek disini
        $query = $this->db->query("SELECT nama_barang FROM barang WHERE nama_barang != '$str2' AND nama_barang = '$str'");
        if($query->num_rows() == 0 ){
           return true;
        } else {
            $this->form_validation->set_message('cek_nama_barang','nama barang sudah ada');
            return false;
        }
        
    }
 
 

    public function update_data()
    {
        $this->load->library('form_validation');
        $v = $this->form_validation;
        $nama_barang_lama = $this->input->post('nama_barang_lama');
        
        $v->set_rules('foto_baru','Foto','trim|callback_cek_foto_update');
        
        //ini cek nama is_unique tapi tidak termasuk dengan nama_barang yang lama
        //sql query Where != nama_barang_lama and Where == nama_barang_baru 
        $v->set_rules('nama_barang_baru','nama barang','trim|required|callback_cek_nama_barang['.$nama_barang_lama.']',array(
            'required'  => '*nama barang harus di isi',
            'is_unique' => '*nama barang yang sama sudah ada',
        ));

        $v->set_rules('harga_beli_baru','nama barang','trim|numeric|required',array(
            'numeric'   => '*hanya boleh di isi dengan angka',
            'required'  => '*tidak boleh di kosongkan'
        ));
        $v->set_rules('harga_jual_baru','nama barang','trim|numeric|required',array(
            'numeric'   => '*hanya boleh di isi dengan angka',
            'required'  => '*tidak boleh di kosongkan'
        ));
        $v->set_rules('stok_baru','stok','trim|numeric|required',array(
            'numeric'   => '*hanya boleh di isi dengan angka',
            'required'  => '*tidak boleh di kosongkan'
        ));
        
        if($v->run() === FALSE ){
            // echo 'lmao';
            $data = array(
                'response_edit'  => 'update-error',
                'content'   => 'v_barang',
                'title'     => 'Barang'
            );
            $this->load->view('layouts/v_layout',$data);
        } else {
            $config = array(
                'upload_path'   => './assets/foto/barang/',
                'allowed_types' => 'jpg|png',
                'max_size'      => '100'//kb,
            );

            $this->load->library('upload',$config);
            
            //check foto_baru if empty or not 
            if(!empty($_FILES['foto_baru']['name'])|| $_FILES['foto_baru']['size'] > 0){
                $foto_lama = base_url('assets/foto/barang/'.$this->input->post('nama_foto_lama'));
                if(count($foto_lama) > 0 ){
                    unlink('assets/foto/barang/'.$this->input->post('nama_foto_lama'));   
                }
            }

            if($this->upload->do_upload('foto_baru')){
                $foto   = $this->upload->data()['file_name'];                                
                $i = $this->input;
                $data_baru = array(
                    'nama_barang'   => $i->post('nama_barang_baru'),
                    'harga_beli'    => $i->post('harga_beli_baru'),
                    'harga_jual'    => $i->post('harga_jual_baru'),
                    'stok'          => $i->post('stok_baru'),
                    'foto'          => $foto
                );
                $this->barang_model->update_data($i->post('id_barang'),$data_baru);
                redirect('barang');
            } else {
                $i = $this->input;
                $data_baru = array(
                    'nama_barang'   => $i->post('nama_barang_baru'),
                    'harga_beli'    => $i->post('harga_beli_baru'),
                    'harga_jual'    => $i->post('harga_jual_baru'),
                    'stok'          => $i->post('stok_baru'),
                );
                $this->barang_model->update_data($i->post('id_barang'),$data_baru);
                redirect('barang');
            }

        }
    }






















    

    public function cek_foto_update()
    {
        if(empty($_FILES['foto_baru']['name'])|| $_FILES['foto_baru']['size'] == 0 ){
            return true;
        } else {
            //jika file ada
            $allowed_extensions = array(
                'jpg','png','JPG','PNG','JPEG','jpeg'
            );
            $allowed_types = array(
                IMAGETYPE_PNG,IMAGETYPE_JPEG
            );
            $extension  = pathinfo($_FILES['foto_baru']['name'],PATHINFO_EXTENSION);
            $type       = exif_imagetype($_FILES['foto_baru']['tmp_name']);

            //check rules
            if(!in_array($type,$allowed_types)){
                $this->form_validation->set_message('cek_foto_update','file harus bertipe JPG/PNG');
                return false;
            }
            if(!in_array($extension,$allowed_extensions)){
                $this->form_validation->set_message('cek_foto_update','file harus berekstensi JPG/PNG');
                return false;
            }
            if($_FILES['foto_baru']['size'] > 100000 ){
                $this->form_validation->set_message('cek_foto_update','file tidak boleh melebihi 100 KB');
                return false;
            }
        } 
        return true;
    }


}


?>